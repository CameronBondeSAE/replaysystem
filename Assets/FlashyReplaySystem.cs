﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashyReplaySystem : ReplaySystemBase
{
	public void Start()
	{
		SpawnManager.OnSpawned(gameObject);
	}
}

public class FlashyKeyframe : KeyFrameBase
{
	public Color colour;
}
