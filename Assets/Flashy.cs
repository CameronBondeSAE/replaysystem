﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flashy : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (Random.value > 0.95f)
		{
			GetComponent<Light>().color = new Color(Random.value, Random.value, Random.value);
		}
	}
}
