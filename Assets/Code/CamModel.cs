﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamModel : MonoBehaviour
{
	public Vector3 jumpAmount;
	//	public Vector3 thrustAmount;
	//	public Vector3 little;
	public Vector3 big;
	public float sizeSpeed;
	Transform t;
	Rigidbody rb;

	public Vector3 rotateAmount;
	private bool onGround;
	public Vector3 startingPosition;
	public Quaternion startingRotation;
	public event Action OnDeath;
	public event Action OnPressed;

	// Use this for initialization
	void Start()
	{
		t = GetComponent<Transform>();
		rb = GetComponent<Rigidbody>();

		startingPosition = transform.position;
		startingRotation = transform.rotation;
	}

	public void Jump()
	{
		if (onGround)
		{
			rb.AddForce(jumpAmount, ForceMode.Impulse);
		}
	}

	public void GetBig()
	{
		t.localScale = ChangeVector3(big);
		rb.AddRelativeTorque(rotateAmount);
	}

	public void GetNormal()
	{
		t.localScale = ChangeVector3(Vector3.one);
	}

	private void OnCollisionStay(Collision other)
	{
		onGround = true;
	}

	private void OnCollisionExit(Collision other)
	{
		onGround = false;
	}

	private Vector3 ChangeVector3(Vector3 amount)
	{
		return Vector3.Lerp(t.localScale, amount, sizeSpeed * Time.deltaTime);
	}

	public void Respawn()
	{
		if (OnDeath != null) OnDeath();
//		transform.position = startingPosition;
//		transform.rotation = startingRotation;
//		rb.velocity = Vector3.zero;
//		rb.angularVelocity = Vector3.zero;
	}
}
