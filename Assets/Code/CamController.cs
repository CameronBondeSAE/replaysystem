﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
	public KeyCode keyCode;
	public CamModel CamModel;

	void Update()
	{
		// Jump
		if (Input.GetKeyDown(keyCode))
		{
			CamModel.Jump();
		}
		// GetBig
		if (Input.GetKey(keyCode))
		{
			CamModel.GetBig();
		}
		else // GetNormal
		{
			CamModel.GetNormal();
		}
	}
}
