﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenWall : MonoBehaviour
{
	public AnimationCurve curve;
	public Transform goToMove;
	public float startTime;

	// Use this for initialization
	void Start()
	{
		startTime = Time.timeSinceLevelLoad + transform.position.z/100;
	}

	// Update is called once per frame
	void Update()
	{
		goToMove.transform.localPosition = new Vector3(0,curve.Evaluate(Time.timeSinceLevelLoad - startTime));
		if (Time.timeSinceLevelLoad - startTime > curve.length)
		{
			startTime = Time.timeSinceLevelLoad;
		}
	}
}
