﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
	public GameObject[] prefabs;
	public int number;
	public int minSpacing;
	public float shimmy;
	public float maxScale;
	public float maxHeight;
	public float maxXScale;

//	float newXScale;

	// Use this for initialization
	void Start()
	{
		for (int i = 0; i < number; i++)
		{
//			newXScale = Random.Range(20, maxXScale);
			GameObject newGo = Instantiate(prefabs[Random.Range(0, prefabs.Length)], new Vector3(0, Random.Range(0, maxHeight), i * minSpacing + Random.Range(0, shimmy)),
				Quaternion.identity);
			Vector3 originalScale = newGo.transform.lossyScale;
			originalScale.y = Random.Range(1, maxScale);
//			newXScale = Random.Range(1, maxXScale);
//			originalScale.x = newXScale;
			newGo.transform.localScale = originalScale;
		}
	}

	// Update is called once per frame
	void Update()
	{

	}
}
