﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Cameras;

public class CameraManager : MonoBehaviour
{


	// Use this for initialization
	void Start()
	{
		FindObjectOfType<GameManager>().OnSpawnedPlayer += OnSpawnedPlayer;
	}

	private void OnSpawnedPlayer(GameObject obj)
	{
		GetComponent<FreeLookCam>().SetTarget(obj.transform.GetComponentInChildren<CamModel>().transform); // HACK hard coded to cammodel because main GO doesn't really have a position
	}
}
