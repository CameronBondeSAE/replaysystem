﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public GameObject playerPrefab;
	public GameObject player;

	public event Action<GameObject> OnSpawnedPlayer;

	// Use this for initialization
	void Start()
	{
		SpawnPlayer();
	}

	private void SpawnPlayer()
	{
		player = Instantiate<GameObject>(playerPrefab, transform.position, transform.rotation);
		player.GetComponentInChildren<CamModel>().OnDeath += OnDeath;

		if (OnSpawnedPlayer != null) OnSpawnedPlayer(player);
	}

	private void OnDeath()
	{
		Destroy(player);
		SpawnPlayer();
	}
}
