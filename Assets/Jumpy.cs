﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpy : MonoBehaviour
{
	[SerializeField] private float jumpHeight;

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void FixedUpdate()
	{
		if (Random.value > 0.98f)
		{
			GetComponent<Rigidbody>().velocity = new Vector3(0, jumpHeight, 0);
		}
	}

}
