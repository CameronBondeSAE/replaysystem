﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
	public static event Action<GameObject> OnSpawned;
	public static event Action<GameObject> OnDestroyed;

	public List<GameObject> gameObjects;

	// Use this for initialization
	void Awake()
	{
		OnSpawned += Spawned;
		OnDestroyed += Destroyed;
	}

	private void Destroyed(GameObject obj)
	{
		gameObjects.Remove(obj);
	}

	private void Spawned(GameObject obj)
	{
		gameObjects.Add(obj);
	}

}
