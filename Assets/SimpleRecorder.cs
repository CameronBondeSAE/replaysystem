﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleReplaySystem
{

	public class SimpleRecorder : MonoBehaviour
	{
		public List<KeyFrameBase> Keyframes;
		KeyFrameBase item;

		private void Start()
		{
			
		}

		public void AddKeyframe()
		{
//			item = new MadeUpShitKeyframe();
//			item.TimeStamp = Time.timeSinceLevelLoad;
//			Keyframes.Add(item);
		}
	}

	[Serializable]
	public class KeyFrameBase
	{
		public float TimeStamp;
	}
	[Serializable]
	public class TransformKeyframe : KeyFrameBase
	{
		public Vector3 position;
		public Quaternion rotation;
	}
}
