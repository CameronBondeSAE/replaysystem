﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using Object = System.Object;

public class ReplayManager : MonoBehaviour
{
	public GameObject ReplayRecorderPrefab;
	public ReplayRecorderBase ReplayRecorder;

	public GameObject ReplayPlayerPrefab;
	public List<ReplayPlayer> ReplayPlayers;

	public GameObject currentPlayer;
	public float restartDelay;

	public void Start()
	{
		// Setup replay recording
		ReplayRecorder = Instantiate(ReplayRecorderPrefab).GetComponent<ReplayRecorderBase>(); // Note I'm not spawning a RespawnRecorder/Player variable directly, because I was telling the flipping prefab itself to do stuff for ages...
		ReplayRecorder.target = currentPlayer.transform;
//		currentPlayer.GetComponentInChildren<JetpackManager>().OnAnyJetpackFired += OnPlayerBeganMoving;
//		currentPlayer.GetComponentInChildren<Death>().OnDie += OnPlayerDiedHandler;

		// Stop when player gets respawned, but DON'T stop recording even though they're dead (it should stop logging the distance though on death)
//		OnRestartSinglePlayer += delegate (object sender, EventArgs args)
//		{
//			ReplayRecorder.StopRecording();
//			foreach (ReplayPlayer replayPlayerItem in ReplayPlayers)
//			{
//				replayPlayerItem.StopPlayback();
//			}
//		};
		ReplayRecorder.InitialiseTargetForRecording(currentPlayer);

	}


	private void OnPlayerBeganMoving(object o, EventArgs eventArgs)
	{
//		if (playerMoved == false)
//		{
//			checkDistance = true;
//
//			ReplayRecorder.StartRecording();
//			foreach (ReplayPlayer replayPlayerItem in ReplayPlayers)
//			{
//				replayPlayerItem.StartPlayback();
//			}
//			playerMoved = true;
//		}
	}

	// If they got a highscore... while dying... TODO: Also on checkpoint/end
	private void OnPlayerDiedHandler(object sender, EventArgs args)
	{

	}



	private ReplayPlayer SpawnReplayPlayerWithCurrentReplay()
	{
		Debug.Log("Spawn new replay player");
		ReplayPlayer replayPlayer;
		replayPlayer = Instantiate(ReplayPlayerPrefab).GetComponent<ReplayPlayer>();
//		replayPlayer.Init(ReplayRecorder.Keyframes, playerPrefab);

		// Keep track of all replays
		// TODO: Make actual highscore instead of just adding one forever!
		ReplayPlayers.Add(replayPlayer);

		return replayPlayer;
	}

	private IEnumerator RestartCoroutine()
	{
		yield return new WaitForSeconds(restartDelay);

		// Record right up to respawning. The BEST DISTANCE stops on death though, but I want the replays to still show the aftermath
		ReplayRecorder.StopRecording();

//		Restart();
	}

}

#region Support classes

// Hack to get around Unity not tagging V2 and V3 as serialisable
internal sealed class Vector3SerializationSurrogate : ISerializationSurrogate
{
	// Method called to serialize a Vector3 object
	public void GetObjectData(Object obj, SerializationInfo info, StreamingContext context)
	{
		Vector3 v3 = (Vector3)obj;
		info.AddValue("x", v3.x);
		info.AddValue("y", v3.y);
		info.AddValue("z", v3.z);
		//	Debug.Log(v3);
	}



	// Method called to deserialize a Vector3 object
	public Object SetObjectData(Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
	{
		Vector3 v3 = (Vector3)obj;
		v3.x = (float)info.GetValue("x", typeof(float));
		v3.y = (float)info.GetValue("y", typeof(float));
		v3.z = (float)info.GetValue("z", typeof(float));
		obj = v3;
		return obj; // Formatters ignore this return value //Seems to have been fixed!
	}
}

internal sealed class Vector2SerializationSurrogate : ISerializationSurrogate
{
	// Method called to serialize a Vector2 object
	public void GetObjectData(Object obj, SerializationInfo info, StreamingContext context)
	{
		Vector2 v2 = (Vector2)obj;
		info.AddValue("x", v2.x);
		info.AddValue("y", v2.y);
		//        Debug.Log(v2);
	}



	// Method called to deserialize a Vector3 object
	public Object SetObjectData(Object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
	{
		Vector2 v2 = (Vector2)obj;
		v2.x = (float)info.GetValue("x", typeof(float));
		v2.y = (float)info.GetValue("y", typeof(float));
		obj = v2;
		return obj; // Formatters ignore this return value //Seems to have been fixed!
	}
}

#endregion

