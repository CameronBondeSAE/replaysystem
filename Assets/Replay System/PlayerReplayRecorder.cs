﻿using System;
using UnityEngine;

public class PlayerReplayRecorder : ReplayRecorderBase
{
	public CamModel[] camModels;


	private ControllerKeyframe controllerKeyframe;


	public override void InitialiseTargetForRecording(GameObject playerTarget)
	{
		target = playerTarget.transform;
		camModels = target.GetComponentsInChildren<CamModel>();


		// Start recording only when they FIRST MOVE (They can sit there as long as they like)
		foreach (CamModel camModel in camModels)
		{
//			jetpack.OnJetpackFiring += delegate(object sender1, EventArgs e)
//									   {
//										   if (isRecording)
//										   {
//											   RecordControllerKeyframe(true, ((Jetpack)sender1).Index);
//										   }
//									   };
//
//			jetpack.OnJetpackIdle += delegate(object sender1, EventArgs e)
//									 {
//										 if (isRecording)
//										 {
//											 RecordControllerKeyframe(false, ((Jetpack)sender1).Index);
//										 }
//									 };
		}

		// This records that a death happened AND ALSO an absolute keyframe to make sure the death, distance and time are all accurate at the end
		target.GetComponentInChildren<CamModel>().OnDeath += delegate
														  {
															  RecordPhysicsKeyFrame();
															  RecordDeathKeyframe();
														  };

//		target.GetComponentInChildren<Boost>().OnBoost += delegate { RecordBoostKeyframe(); };
	}


	#region Recording

	//*****************************************************************
	// Recording
	//*****************************************************************
	public override void StartRecording()
	{
		// Record the first controller keyframe manually in case the jets were already activated on start
		foreach (CamModel camModel in camModels)
		{
//			RecordControllerKeyframe(jetpack.active, jetpack.Index);
		}
		recordingCoroutine = StartCoroutine(RecordingPhysicsCoroutine());
	}


	private void RecordControllerKeyframe(bool _turnedOn, ushort _jetpackIndex)
	{
		controllerKeyframe = new ControllerKeyframe() { turnedOn = _turnedOn, jetpackIndex = _jetpackIndex };
		AddKeyframe(controllerKeyframe);
	}



	private void RecordBoostKeyframe()
	{
		AddKeyframe(new BoostKeyframe());
	}



	private void RecordDeathKeyframe()
	{
		AddKeyframe(new DeathKeyframe());
	}




	#endregion

	// Hack area
	//	private void Update()
	//	{
	//		if (Input.GetKeyDown(KeyCode.F1))
	//		{
	//			// Start recording
	//			if (isRecording == false)
	//			{
	//				StartRecording();
	//			}
	//			else // Stop recording
	//			{
	//				StopRecording();
	//			}
	//		}
	//		if (Input.GetKeyDown(KeyCode.S))
	//		{
	//			//SaveBufferToFile();
	//		}
	//
	//		if (Input.GetKeyDown(KeyCode.L))
	//		{
	//			LoadBufferFromFile();
	//		}
	//	}
}


#region Keyframe types
[Serializable]
public class ControllerKeyframe : KeyFrameBase
{
	public ushort jetpackIndex;
	public bool turnedOn;
}

[Serializable]
public class DeathKeyframe : KeyFrameBase
{
}

[Serializable]
public class BoostKeyframe : KeyFrameBase
{
}

#endregion


