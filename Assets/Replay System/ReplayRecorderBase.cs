﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class ReplayRecorderBase : MonoBehaviour
{
	public List<KeyFrameBase> Keyframes;
	public Transform target;
	protected float startRecordingTime = 0;
	public bool isRecording = false;

	// For regular absolute position/rotation etc correction recording
	public float newPhysicsKeyframeDelay = 0.5f;
	public float newTransformKeyframeDelay = 0.5f;
	public Coroutine recordingCoroutine;


	private void Start()
	{
		// Setup
		Keyframes = new List<KeyFrameBase>();
	}


	public virtual void InitialiseTargetForRecording(GameObject playerTarget)
	{

	}

	public virtual void StartRecording()
	{
		if (isRecording)
		{
			Debug.LogWarning("Trying to record, but already recording : Stopping and starting");
			StopRecording();
		}

		//		StopPlayback();

		isRecording = true;
		startRecordingTime = Time.timeSinceLevelLoad;
		Keyframes.Clear();
}
	public void StopRecording()
	{
		isRecording = false;
		if (recordingCoroutine != null)
		{
			StopCoroutine(recordingCoroutine);
		}
	}

	protected IEnumerator RecordingTransformCoroutine()
	{
		while (true)
		{
			RecordTransformKeyFrame();
			yield return new WaitForSeconds(newTransformKeyframeDelay);
		}
	}

	protected IEnumerator RecordingPhysicsCoroutine()
	{
		while (true)
		{
			RecordPhysicsKeyFrame();
			yield return new WaitForSeconds(newPhysicsKeyframeDelay);
		}
	}

	protected void AddKeyframe(KeyFrameBase keyframe)
	{
		keyframe.timeStamp = Time.timeSinceLevelLoad - startRecordingTime;
		Keyframes.Add(keyframe);
	}

	protected TransformKeyframe transformKeyframe;
	protected void RecordTransformKeyFrame()
	{
//		physicsKeyframe = new PhysicsKeyframe() { position = target.position, zRotation = target.rotation.eulerAngles.z, velocity = target.GetComponent<PhysicsObject>().Velocity, angularZVelocity = target.GetComponent<PhysicsObject>().AngularVelocity.z };
		AddKeyframe(transformKeyframe);
	}

	protected PhysicsKeyframe physicsKeyframe;
	protected void RecordPhysicsKeyFrame()
	{
//		physicsKeyframe = new PhysicsKeyframe() { position = target.position, zRotation = target.rotation.eulerAngles.z, velocity = target.GetComponent<PhysicsObject>().Velocity, angularZVelocity = target.GetComponent<PhysicsObject>().AngularVelocity.z };
		AddKeyframe(physicsKeyframe);
	}

	#region FileOps

	public void LoadBufferFromFile()
	{
		try
		{
			using (Stream stream = File.Open("BestReplay.bin", FileMode.Open))
			{
				BinaryFormatter bin = new BinaryFormatter();
				// 2. Construct a SurrogateSelector object
				SurrogateSelector ss = new SurrogateSelector();
				Vector2SerializationSurrogate v2ss = new Vector2SerializationSurrogate();
				ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), v2ss);
				// 5. Have the formatter use our surrogate selector
				bin.SurrogateSelector = ss;


				Keyframes = (List<KeyFrameBase>)bin.Deserialize(stream);

				Debug.Log("ReplayRecorder: Loaded replay");
			}
		}
		catch (IOException)
		{
		}
	}



	//
	//	public void LogEvent(object o, EventArgs e)
	//	{
	//		eventEntry = new EventEntry();
	//
	//		eventEntry.e = e;
	//		eventEntry.timeStamp = Time.timeSinceLevelLoad;
	//		//		events.Add(eventEntry);
	//	}


	public MemoryStream CreateReplayBinaryBlob()
	{
		// When I want to upload to the server I need to binary blob CHECK: Probably should separate sav
		using (MemoryStream memoryStream = new MemoryStream())
		{
			BinaryFormatter bin = new BinaryFormatter();

			// 2. Construct a SurrogateSelector object
			SurrogateSelector ss = new SurrogateSelector();
			Vector2SerializationSurrogate v2ss = new Vector2SerializationSurrogate();
			ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), v2ss);
			// 5. Have the formatter use our surrogate selector
			bin.SurrogateSelector = ss;

			return memoryStream;
		}
	}

	public void SaveBufferToFile()
	{
		try
		{
			using (Stream stream = File.Open("BestReplay.bin", FileMode.Create))
			{
				BinaryFormatter bin = new BinaryFormatter();

				// 2. Construct a SurrogateSelector object
				SurrogateSelector ss = new SurrogateSelector();
				Vector2SerializationSurrogate v2ss = new Vector2SerializationSurrogate();
				ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), v2ss);
				// 5. Have the formatter use our surrogate selector
				bin.SurrogateSelector = ss;

				bin.Serialize(stream, Keyframes);

				Debug.Log("ReplayRecorder: Saved replay");
			}
		}
		catch (IOException)
		{
		}
	}

	#endregion

}

#region Keyframe types

//
//[Serializable]
//public class EventEntry
//{
//    public EventArgs e;
//    public float timeStamp;
//}

[Serializable]
public class KeyFrameBase
{
	public float timeStamp;
}

[Serializable]
public class TransformKeyframe : KeyFrameBase
{
	[SerializeField]
	public Vector2 position;
	public float zRotation;
}

[Serializable]
public class PhysicsKeyframe : KeyFrameBase
{
	[SerializeField]
	public Vector2 position;
	public float zRotation;
	[SerializeField]
	public Vector2 velocity;
	public float angularZVelocity;
}
#endregion



#region Disabled
//	public void OnGUI()
//	{
//		GUILayout.Label("isRecording = " + isRecording);
//	}



//
//
//	private var client = new HttpClient();
//
//
//	public void send_some_shit(string s1)
//    {
//		client.DefaultRequestHeaders.Add("User-Agent","WJE/1.0" );
//
//        var values = new List<KeyValuePair<string, string>>();
//
//	    client.DefaultRequestHeaders.Add("Content-Transfer-Encoding","base64");
//                var s3 = System.Convert.ToBase64String( s1 );
//                // Console.WriteLine( "cs, post-base64'd length = " +
//    s3.Length );
//                // Console.WriteLine( "cs, base64'd: " + s3 );
//
//                values.Add(new KeyValuePair<string, string>("banana",
//    "yellow"));
//                values.Add(new KeyValuePair<string, string>("userID",
//    "Jar Jar Binks Mum" ));
//
//                var content = new FormUrlEncodedContent( values );
//
//                var response = await client.PostAsync(
//    "10.1.1.191/wje/api.php <http://10.1.1.191/wje/api.php>", content );
//
//
//                    //.PostAsJsonAsync("http://10.1.1.111/proj/mas/v3/api/",a_csharp_object_to_json_serialize );
//                    // The PostAsJsonAsync method serializes an object to JSON and then sends the JSON payload in a POST request.
//                    // To send XML, use the PostAsXmlAsync
//
//                var responseString = await
//    response.Content.ReadAsStringAsync();
//
//                if ( System.Net.HttpStatusCode.OK != response.StatusCode )
//                {
//                    Console.WriteLine( "status code: " +
//    response.StatusCode + " was not OK" );
//                }
//            else
//                {
//                            do stuff with response!!
//                }
//
//    }
//
#endregion