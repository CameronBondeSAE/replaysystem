﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class ReplayPlayer : MonoBehaviour
{
	bool isPlaying = false;
	public List<KeyFrameBase> playbackKeyframes;
	public Transform playerReplayGhost;
//	public JetpackManager TargetJetpackManager; // CHECK: TODO: Swap to generic controller input

	Coroutine playbackRecordingCoroutine;



	public void Init(List<KeyFrameBase> _playbackKeyframes, GameObject _playerPrefab)
	{
		//target = _replayPlayerTarget.transform;
		playbackKeyframes = new List<KeyFrameBase>(_playbackKeyframes); // Make a new list because it was storing it as a reference to the original ReplayRecorder

		SpawnPlayerGhost(_playerPrefab);

		// Just to kill the body after a few seconds
//		playerReplayGhost.GetComponentInChildren<Death>().OnDie += delegate (object sender, EventArgs args)
//																   {
//																	   StartCoroutine(OnDied());
//																   };
	}



	private IEnumerator OnDied()
	{
		yield return new WaitForSeconds(3);
		playerReplayGhost.gameObject.SetActive(false);
	}




	// Hack area
	//	private void Update()
	//	{
	//		if (Input.GetKeyDown(KeyCode.F2))
	//		{
	//			if (isPlaying == false)
	//			{
	//				StartPlayback();
	//			}
	//			else
	//			{
	//				StopPlayback();
	//			}
	//		}
	//	}


	public void SpawnPlayerGhost(GameObject playerPrefab)
	{
		// Spawn actual player ghost
		playerReplayGhost = (Instantiate(playerPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<Transform>();
		playerReplayGhost.gameObject.name = "Player - Replay "; // TODO support multiplayer
																//playerReplayGhost[index].SetActive(false); // HACK: Can't turn this off BEFORE setup as GetComponent doesn't find inactives!


		// Link up
//		TargetJetpackManager = playerReplayGhost.GetComponentInChildren<JetpackManager>();

		// HACK: Disable local controller, collisions etc
		// TODO: Should I just send a message to the object and let specific components deal with this themselves? Especially due to the dynamic spawning of jetpacks/props/skins etc
//		playerReplayGhost.GetComponent<CollisionsAndTriggersEvent>().enabled = false;
//		playerReplayGhost.GetComponent<CollisionsAndTriggersEvent>().enableManual = false;

		
		// Remove ALL colliders so no collisions ever
		// CHECK: problems with jetpack against walls TODO: Do the same for explosions etc)
		foreach (Collider colliderItem in playerReplayGhost.GetComponentsInChildren<Collider>())
		{
			if (colliderItem.isTrigger == false)
			{
				Destroy(colliderItem);
			}
		}
		playerReplayGhost.GetComponentInChildren<AudioListener>().enabled = false;
//		playerReplayGhost.GetComponentInChildren<BlockedJetsBoost>().enabled = false;

		// Change colour of arrow! TODO: Make it more different/clever/customisable
//		playerReplayGhost.GetComponentInChildren<DirectionIndicator>().enabled = false;
		//		target.GetComponentInChildren<ExplodeJetpack>().enabled = false;

//		foreach (ControllerBase controllerBase in playerReplayGhost.GetComponentsInChildren<ControllerBase>())
//		{
//			controllerBase.enabled = false;
//		}

	}




	#region Playback
	//*****************************************************************
	// Playback
	//*****************************************************************

	public void StartPlayback()
	{
		playerReplayGhost.gameObject.SetActive(true);

		playerReplayGhost.GetComponent<Rigidbody>().isKinematic = false; // HACK: Abstract physics

		if (playbackKeyframes.Count <= 0)
		{
			Debug.LogWarning("StartPlayback: No frames");
			return;
		}

		if (isPlaying)
		{
			Debug.LogWarning("Trying to play, but already playing : Stopping and starting");
			StopPlayback();
		}
		//StopRecording();

		isPlaying = true;
		//target.gameObject.SetActive(true); // Turn on
		playbackRecordingCoroutine = StartCoroutine(PlayBackCoroutine());
	}


	public void StopPlayback()
	{
		//StopRecording();

		isPlaying = false;
		if (playbackRecordingCoroutine != null)
		{
			// Turn off
			StopCoroutine(playbackRecordingCoroutine);
			//target.gameObject.SetActive(false);

			playerReplayGhost.GetComponent<Rigidbody>().isKinematic = true; // HACK: Abstract physics
		}
	}


	IEnumerator PlayBackCoroutine()
	{
		// HACK to reset the 'allowDeath' state, from the last death. TODO: Do a proper respawn system
//		playerReplayGhost.GetComponent<Respawnable>().Respawn();
		//		target.GetComponentInChildren<Death>().Respawned();
		//		target.GetComponentInChildren<Death>().allowDeath = true;
		//		target.gameObject.SetActive(true);

		Vector3 keyFrameRotation = new Vector3();

		int keyframeIndex = 0;
		float playbackTimerStart = Time.timeSinceLevelLoad;
		float playbackTimer = 0;

		while (keyframeIndex < playbackKeyframes.Count)
		{
			playbackTimer = Time.timeSinceLevelLoad - playbackTimerStart;

			//			Debug.Log("playbackTimer: " + playbackTimer + " : index = "+keyframeIndex + " : count = "+Keyframes.Count);

			if (playbackKeyframes[keyframeIndex] != null)
			{
				if (playbackTimer > playbackKeyframes[keyframeIndex].timeStamp)
				{
					// PhysicsKeyFrame
					PhysicsKeyframe physicsKeyframe = playbackKeyframes[keyframeIndex] as PhysicsKeyframe;
					if (physicsKeyframe != null)
					{
						playerReplayGhost.position = physicsKeyframe.position;
						keyFrameRotation.z = physicsKeyframe.zRotation;
						playerReplayGhost.rotation = Quaternion.Euler(keyFrameRotation);

//						playerReplayGhost.GetComponent<PhysicsObject>().Velocity = physicsKeyframe.velocity;
//						playerReplayGhost.GetComponent<PhysicsObject>().AngularVelocity = new Vector3(0, 0, physicsKeyframe.angularZVelocity); // HACK converting from 3d to 2d all the time                        
					}
					else
					{
						// ControllerKeyFrame
						ControllerKeyframe controllerKeyframe = playbackKeyframes[keyframeIndex] as ControllerKeyframe;

						if (controllerKeyframe != null)
						{
							if (controllerKeyframe.turnedOn)
							{
//								TargetJetpackManager.Jetpacks[controllerKeyframe.jetpackIndex].TurnThrustOn(this, EventArgs.Empty);
							}
							else
							{
//								TargetJetpackManager.Jetpacks[controllerKeyframe.jetpackIndex].TurnThrustOff(this, EventArgs.Empty);
							}
						}
						else
						{
							// DeathKeyFrame
							DeathKeyframe deathKeyframe = playbackKeyframes[keyframeIndex] as DeathKeyframe;

							if (deathKeyframe != null)
							{
//								playerReplayGhost.GetComponentInChildren<Death>().Die();
							}
							else
							{
								// BoostKeyFrame
								BoostKeyframe boostKeyframe = playbackKeyframes[keyframeIndex] as BoostKeyframe;

								if (boostKeyframe != null)
								{
//									playerReplayGhost.GetComponentInChildren<Boost>().Activate();
								}
							}

						}
					}

					keyframeIndex++;
				}
			}

			yield return 0;

			// End of recording. Restart TODO: Don't restart, event instead
			//			if (keyframeIndex > playbackKeyframes.Count - 1) // CHECK: Need -1?
			//			{
			//				keyframeIndex = 0;
			//				playbackTimerStart = Time.timeSinceLevelLoad;
			//			}
		}


		keyframeIndex = 0;
		playbackTimerStart = Time.timeSinceLevelLoad;


		//		target.gameObject.SetActive(false); // CHECK: Seems weird here

		yield return 0;
	}

	#endregion

	//	public void OnGUI()
	//	{
	//		GUILayout.Label("isPlaying = " + isPlaying);
	//	}
}